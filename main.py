#identify all athletes that ran 800, 1600, and 3200 all 4 years, retrieve AtheleteID
#identify top time for each year for each athlete - retreive time, date, eventID
#plot time progression for each distance by gender

import os
import pandas as pd
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import plotly.figure_factory as ff
#from plotly import tools
from flask import Flask
from google.cloud import datastore
#import time

def create_client(project_id):
    return datastore.Client(project_id)

project_id = 'dist-app-1'
client = create_client(project_id)

def racetime(x, no_dec=False):
    from datetime import timedelta
    import numpy as np
    if np.isnan(x): return np.NAN
    d = timedelta(seconds=x/1000)
    [h, m, s] = str(d).split(':')
    s = s.split('.')[0]
    x = d.microseconds/100000
    if no_dec:
        return str(m) + ':' + str(s)
    else:
        return str(m) + ':' + str(s) + '.' + str(x)
    
def racedatetime(x):
    from datetime import datetime
    import numpy as np
    if np.isnan(x): 
        return np.NAN
    else:
        return datetime.fromtimestamp(x/1000.0)

def time_to_units(timestr):
    import numpy as np
    try:
        ftr = [60,1]
        return int(sum([a*b for a,b in zip(ftr, map(float,timestr.split(':')))])*1000)
    except:
        return np.NAN

def slice_df(event, best_time_str, best_time_yr, similarity, gender):
    
    import numpy as np
    
    hs_years = ['Fr', 'So', 'Jr', 'Sr']
    try:
        yr_index = hs_years.index(best_time_yr)
    except:
        print('year abbreviation entered incorrectly, use Fr, So, or Jr')
        return pd.DataFrame()

    if yr_index == 3:
        print('year for best time must be Jr or lower')
        return pd.DataFrame()
    
    best_units = time_to_units(best_time_str)
    
    if np.isnan(best_units):  # return None if time is malformed
        print('time entered incorrectly')
        return pd.DataFrame()
    
    grade = best_time_yr + '_Units'
    
    print('querying for %s, %s, %s, %s' %(event, gender, grade, best_units))
    
    # google datastore
    kind = 'Runner-Event'
    query = client.query(kind=kind)
    query.add_filter('EventCode', '=', event)
    query.add_filter('Gender', '=', gender)
    query.add_filter(grade, '<=', best_units + int(similarity * 1000))
    query.add_filter(grade, '>=', best_units - int(similarity * 1000))
    results = query.fetch()
    df = pd.DataFrame(list(results))
    
    print("total hits = ", results.num_results)
    
    def format_nans(value):
        import numpy as np
        if value=='nan': return np.NAN
        return value
      
    # add columes with milliseconds converted to times (text)
    if len(df) != 0:
        for grade in hs_years:
            col = grade + '_Units'
            df[col] = [format_nans(x) for x in df[col]]
            new_col = grade + '_Time'
            df[new_col] = [racetime(x) for x in df[col]]

    return df

server = Flask(__name__)
server.secret_key = os.environ.get('SECRET_KEY', '2 \xfd5\x06\x97\xe5\xe5\x8c \n/\x06\xaf5\xeb/\n\x1d\xdc\xda\x1eej')
app = dash.Dash(__name__, server=server)

#app = dash.Dash()

my_css_url = 'https://codepen.io/chriddyp/pen/bWLwgP.css'

app.css.append_css({
    "external_url": my_css_url
})

app.layout = html.Div([
    html.Div([
        html.Div([
            html.H1(
                'High School Distance Race Time Projections',
#                className='eight columns',
                style={'text-align': 'center'},),],),
        html.Div([
            html.Label('Race Length'),
            dcc.Dropdown(
                id='race',
                options=[{'label': i, 'value': i} for i in ['800m', '1600m', '3200m']],
                value='1600m'
            )
        ],
            style={'width': '150px', 'display': 'inline-block', 'margin': '2%'}),
        html.Div([
            html.Label('High School Grade'),
            dcc.Dropdown(
                id='grade',
                options=[{'label': i, 'value': i} for i in ['Fr', 'So', 'Jr']],
                value='Fr'
            )
        ],
            style={'display': 'inline-block'}),
        html.Div([
            html.Label('Gender'),
            dcc.Dropdown(
                id='gender',
                options=[{'label': i, 'value': i} for i in ['girls', 'boys']],
                value='girls'
            )
        ],
            style={'width': '100px', 'display': 'inline-block', 'margin': '2%'}),
        html.Div([
            html.Label('Best Time'),
            dcc.Input(
                id='time',
                placeholder='Enter a race time i.e, 5:05...',
                type='text',
                value='5:30'
            )
        ],
            style={'display': 'inline-block', 'margin': '2%'}),
        html.Div([
            html.Label('Range in Seconds'),
            dcc.Input(
                id='similarity',
                placeholder='Enter a time range i.e., 1.0 seconds',
                type='text',
                value='3.0'
            )
        ],
            style={'display': 'inline-block', 'margin': '2%'}),
        html.Div([
            html.Label('Plot Stats or Individual Runners'),
            dcc.RadioItems(
                id='bool_stats',
                options=[{'label': i, 'value': j} for i,j in zip(['Stats', 'Individuals'], [True, False])],
                value=True,
                labelStyle={'display': 'inline-block', 'margin': '2%'}
            )
        ],
            style={'display': 'inline-block'}),
    ]),
    
    dcc.Graph(id='historical_progression')
    
])

@app.callback(
    dash.dependencies.Output('historical_progression', 'figure'),
    inputs=
    [dash.dependencies.Input('race', 'value'),
     dash.dependencies.Input('grade', 'value'),
     dash.dependencies.Input('time', 'value'),
     dash.dependencies.Input('similarity', 'value'),
     dash.dependencies.Input('gender', 'value'),
     dash.dependencies.Input('bool_stats', 'value')
    ])
def update_graph(race, grade, time, similarity, gender, bool_stats=True):

    print("calling with time " + str(time))
            
    grades = ['Fr', 'So', 'Jr', 'Sr']
    
    try: similarity = float(similarity)
    except: similarity = 1.0
           
    df = slice_df(race, time, grade, similarity, gender)
    
    if df.empty:
        return
            
    percentiles = [1,5,10,15,20,30,40,50,60,70,80]
    cols = [x + '_Units' for x in grades]
    def create_percentiles(df, col_name, percentiles):
        import numpy as np
        units_list = [np.percentile(df[col_name].dropna(), x) for x in percentiles] # calculate the percentiles for the prediction year
        times_list = [racetime(x) for x in units_list]
        col_abv = col_name[:2]
        return pd.DataFrame(np.transpose([units_list, times_list]), index=percentiles, columns=[col_abv+'_Units', col_abv+'_Time'])
    stats =  pd.concat([create_percentiles(df, col_name, percentiles) for col_name in df[cols].columns], axis=1)
                
    #create stats table to  display next year data in table
    grades = ['Fr', 'So', 'Jr', 'Sr']

    # determine whether to print indidivual results or statistics only
    if bool_stats:
        plot_df = stats.copy()
    else:
        plot_df = df.copy()
    
    print(plot_df.head(3))
    
    grade_index = grades.index(grade)
    next_grade = grades[grade_index + 1]
    stats_table = stats[[next_grade+'_Units', next_grade+'_Time']].reset_index()
    stats_table['Percentile'] = stats_table['index']
    del stats_table['index']
               
    # create plots
    new_cols= [x + '_DT' for x in grades]
    for new_col, col in zip(new_cols, cols):
        plot_df[new_col] = [racedatetime(float(x)) for x in plot_df[col]]
        
    traces = []
    x_data=[0,1,2,3]
    text_cols = [x + '_Time' for x in grades]
    
    max_plots=200
    total_plots = len(plot_df)
    if total_plots > max_plots:
        total_plots = max_plots

    for i in range(total_plots):
        y_data = plot_df.iloc[i][new_cols]
        text_1 = plot_df.iloc[i][text_cols]
        if bool_stats:
            text_2 = str(percentiles[i]) + '%tile'
        else:
            text_2 = ''
        text = text_2 + ': ' + text_1
        traces.append(go.Scatter(
            y=y_data,
            x=x_data,
            text=text,
            mode='lines',
            connectgaps=True,
            hoverinfo='text',
            xaxis='x2', yaxis='y2'))
    
    # create graphic - start with table
    fig = ff.create_table(stats_table[['Percentile',next_grade+'_Time']], height_constant=30)

    # then add scatter plot
    fig['data'].extend(go.Data(traces))

    # Edit layout for subplots
    fig.layout.xaxis.update({'domain': [0, .2]})
    fig.layout.xaxis2.update(dict(domain=[0.3, 1.], title='Year in High School', ticktext=['Fr', 'So', 'Jr', 'Sr'], tickvals=[0,1,2,3]))

    # The graph's yaxis MUST BE anchored to the graph's xaxis
    fig.layout.yaxis2.update(dict(anchor='x2', title='Time', tickformat='%M:%S', hoverformat='%M:%S.%L'))

    fig_title = 'Time Progression for Similar Runners With a Time of %s as a %s in the %s' %(time, grade, race)
    fig_subtitle = 'There are %s Similar Runners within +/- %s Seconds' %(len(df), similarity)
    table_title = 'Predicted Time for Next Year'
    fig.layout.update({'showlegend': False})
    fig.layout.margin.update({'t':50, 'b':50, 'r':25, 'l':25})
    fig.layout.annotations.extend([dict(x=.6, y= 1.15, xref='paper', yref='paper', text=fig_title, showarrow=False, font=dict(size=14))])
    fig.layout.annotations.extend([dict(x=.6, y= 1.08, xref='paper', yref='paper', text=fig_subtitle, showarrow=False)])
    fig.layout.annotations.extend([dict(x=.03, y= 1.15, xref='paper', yref='paper', text=table_title, showarrow=False, font=dict(size=14))])
    
    return fig
    
if __name__ == '__main__':
	# Attach Python Cloud Debugger
	try:
	  import googleclouddebugger
	  googleclouddebugger.enable()
	except ImportError:
	  pass
	app.server.run()